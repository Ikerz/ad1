#!/usr/bin/env python 
from random import shuffle
import sys
if (len(sys.argv)!=4):
	print("usage: executable n k output.txt\t")
	sys.exit(0)
n = int(sys.argv[1])
k = int(sys.argv[2])
output = sys.argv[3]
if(k>n):
	print("input error (k>n)")
	sys.exit(0)
x = [i for i in range(1,n+1)]

shuffle(x)

with open(output, "a") as myfile:
    		myfile.write(str(n)+"\n")
    		for number in x:
    			myfile.write(str(number)+"\n")
    		myfile.write(str(k))


